
# Simple Cache

Simple Cache is the interview-followup homework that tries to address the following requirements:

```
Write a simple in memory cache that

Implements get and put methods

Allows to select none, one or both below retention polices

Implement retention policy that removes item when the period has passed

Implement retention policy that limits cache size and removes first inserted item when defined cache size is exceeded

More retention policies are going to be added in the future
```

## Author
[Nikodem Karbowy](http://wgoracejwodzie.company)